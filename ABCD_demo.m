% Demo for Asymmetric Binary Component Decomposition (ABCD). 
% Constructs a matrix according to C = ZW^T with W having 
% real entries and Z having entries either 0 or +1. Then noise is added,
% and then we compute its ABCD.
% 
% The user may specify: the dimensions n and m of C, the rank r, the noise 
% level, the algorithm for SSCD and ASCD, the parameters epsilon and 
% trace_tol.

clear all; 
clc

addpath('utils')
addpath('main functions')

% Some of the parameters for the experiment: 
n = 30; % Z is n-by-r 
m = 40; % W is m-by-r 
r = 4;  % C = ZW^T is n-by-m
noiselevel = 0.02; % The ratio |N|^2_F/|X|_F^2, where N is the noise and 
                   % X is the ground truth signal.
                   
disp('Randomly constructing C = ZW^T where Z is binary, W = randn(m,r).'); 
Z = round(rand(n,r))
W = randn(m,r);

disp('Press any button to continue.'); 
pause 

disp('Checking whether Z satisfies Schur independence.'); 
schur_flag = Check_Schur_indep_binary(Z);
if(schur_flag == -1)
    sprintf('Z is not Schur independent. Program will terminate now.')
    return;
end

Noise = randn(n,m);

Cno = Z*W.'; % Noiseless input

disp('Adding Gaussian noise to C.');       
bruit = noiselevel * Noise / norm(Noise,'fro') * norm(Cno,'fro');
C = Cno + bruit; 

% Default option of ASCD_for_ABCD, you can play and change them
options = []; 
% options.epsilon = 5*10^(-2);
% options.trace_tol = 3;
% Solvers for the SDP subproblems
% options.sscd_algo = 'IPM'; % (default, can be changed to IPM) 
% options.ascd_algo = 'IPM'; % (default, can be changed to IPM) 

disp('Running the SDP-based algorithm for ABCD...'); 
[Sign_comps, W_approx] = ASCD_for_ABCD(C, r, options);

disp('Approximate ABCD of C computed, as Z'' W''^T where Z is binary.'); 
test1 = Sign_comps + 1; % Test if there is a column of all -1.
idx = find(sum(test1) == 0);
phi = -1;

if(isempty(idx))
    test2 = Sign_comps - 1; % Test if there is a column of all +1.
    idx = find(sum(test2) == 0);
    phi = 1;
end

if(isempty(idx))
    sprintf('No column of -1 or +1 was found')
    idx = r+1;
end

W_approx_res = swap_columns(W_approx,idx,r);
Sign_comps_res = swap_columns(Sign_comps,idx,r);
e_m = ones(m,1);
e_n = ones(n,1);
x = pinv(W_approx_res(:,1:r))*(phi*W_approx_res(:,r+1) + e_m);

Z_retr = zeros(n,r);
W_plus = zeros(m,r);
for i=1:r
    temp_vec = round(1/2*(x(i)*Sign_comps_res(:,i) + e_n));
    max_val = max(temp_vec);
    min_val = min(temp_vec);
    temp_vec( (temp_vec == max_val) ) = 1;
    temp_vec( (temp_vec == min_val) ) = 0;
    Z_retr(:,i) = temp_vec; 
    W_plus(:,i) = x(i).*W_approx_res(:,i);
end
C_approx = Z_retr*W_plus.';

Sign_Z = 2*Z - 1;
Sign_Zretr = 2*Z_retr - 1;

% Cost is equal to the number of wrong elements retrieved for the 
% binary factor.
[perms,cost,signperms] = mismatchpm1(Sign_Zretr,Sign_Z);

binary_asymm_error = norm(Cno - C_approx, 'fro')/norm(Cno, 'fro');

fprintf('Relative error of the aproximation: ||C-Z'' W''^T||_F/||C||_F = %2.2f%%\n', 100*binary_asymm_error);
fprintf('The number of mismatches between Z and Z'' is %1.0f\n', cost); 