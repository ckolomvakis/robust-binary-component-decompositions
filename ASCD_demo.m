% Demo for Asymmetric Sign Component Decomposition (ASCD). 
% Constructs a matrix according to B = SW^T with W having real entries and 
% S having entries either - 1 or +1. Then noise is added, and then we 
% compute its ASCD.
% 
% The user may specify: the dimensions n and m of B, the rank r, the noise 
% level, the algorithm for SSCD and ASCD, the parameters epsilon and 
% trace_tol.

clear all; 
clc

addpath('utils')
addpath('main functions')

% Some of the parameters for the experiment:
n = 30; % S is n-by-r 
m = 40; % W is m-by-r 
r = 4;  % B = SW^T is n-by-m
noiselevel = 0.02; % The ratio |N|^2_F/|X|_F^2, where N is the noise and 
                   % X is the ground truth signal.

disp('Randomly constructing B = SW^T where S has elements in {-1, +1}, W = randn(m,r).');                   
S = 2*round(rand(n,r))-1
W = randn(m,r);

disp('Press any button to continue.'); 
pause 

disp('Checking whether S satisfies Schur independence.'); 
schur_flag = Check_Schur_indep(S);
if(schur_flag == -1)
    sprintf('S is not Schur independent. Program will terminate now.')
    return;
end

Noise = randn(n,m); 

Bno = S*W.'; % Noiseless input

disp('Adding Gaussian noise to B.');  
bruit = noiselevel * Noise / norm(Noise,'fro') * norm(Bno,'fro');
B = Bno + bruit; 

[Ub,~,~] = svd(B); 
U = Ub(:,1:r);

% Default option of ASCD, you can play and change them
options = []; 
% options.epsilon = 10^(-2);
% options.trace_tol = 3;
% Solvers for the SDP subproblems
% options.sscd_algo = 'PG'; % (Default, can be changed to 'IPM')
% options.ascd_algo = 'PG'; % (Default, can be changed to 'IPM')

disp('Running the SDP-based algorithm for ASCD...'); 
[Sign_comps, W_approx] = ASCD(B, r, options);

% Cost is equal to the number of wrong elements retrieved for the 
% binary factor.
[perms,cost,signperms] = mismatchpm1(Sign_comps,S);

B_approx = Sign_comps*W_approx.';
asym_sign_error = norm(Bno - B_approx, 'fro')/norm(Bno, 'fro');

fprintf('Relative error of the aproximation: ||B-S'' W''^T||_F/||B||_F = %2.2f%%\n', 100*asym_sign_error);
fprintf('The number of mismatches between S and S'' is %1.0f\n', cost); 