# Robust Binary Component Decompositions

This code contains demos for the computation of:

1) Symmetric Sign Component Decomposition (SSCD): 
Given a correlation matrix $\mathbf{A} = \displaystyle\sum_{i=1}^r \tau_i \mathbf{s}_i \mathbf{s}^T_i$ with $\mathbf{s}_i \in \{\pm 1\}^{n}$, where the $\mathbf{s}_i$ are Schur independent and the $\tau_i$ form a convex combination of points, compute the components $\mathbf{s}_i$, up to sign and permutation ambiguity, and the $\tau_i$.
You can run the file SSCD_demo.m to run a simple synthetic example. 

1) Asymmetric Sign Component Decomposition (ASCD): 
Given a matrix $\mathbf{B} = \mathbf{S}\mathbf{W}^T$ where $\mathbf{S} \in \{\pm 1\}^{n \times r}$, its columns are Schur independent and 
$\mathbf{W} \in \mathbb{R}^{m \times r}$ is full rank, compute the components $\mathbf{S}$ and $\mathbf{W}$ up to sign and permutation ambiguity.
You can run the file ASCD_demo.m to run a simple synthetic example. 

1) Asymmetric Binary Component Decomposition (ABCD): Given a matrix $\mathbf{C} = \mathbf{Z}\mathbf{W}^T$ with $\mathbf{Z} \in \{0, 1\}^{n \times r}$, which has Schur independent columns and $\mathbf{W} \in \mathbb{R}^{m \times r}$, compute the components $\mathbf{Z}$ and $\mathbf{W}$ up to permutation ambiguity.
You can run the file ABCD_demo.m to run a simple synthetic example. 

This code is associated to the paper Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component Decompositions", which was accepted for presentation at ICASSP 2023. In the folder 'Numerical Experiments', there is the file scalability_test.m where you can run the scalability experiment from the paper.

Please send any comment to christos.kolomvakis@umons.ac.be, thanks! 
