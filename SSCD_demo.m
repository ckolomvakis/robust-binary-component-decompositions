% Demo for Symmetric Sign Component Decomposition (SSCD). 
% Constructs a correlation matrix, perturbs it with  
% standard Gaussian noise and then computes its SSCD. 
% 
% The user may specify: the dimension n of A, the rank r, the noise level,
% the algorithm for SSCD, the parameters epsilon and trace_tol.

clear all
clc

addpath('utils')
addpath('main functions')

% Some of the parameters of the experiment:
n = 30; % S is n-by-r 
r = 4;  % A = S D S^T is n-by-n
noiselevel = 0.05; % The ratio |N|^2_F/|X|_F^2, where N is the noise and 
                   % X is the ground truth signal.  

disp('Randomly construct A = S D S^T: ')                   
S = 2*round(rand(n,r))-1
D = diag(rand(r,1)); 
D = D./(sum(D(:)))

Ano = S*D*S'; % Noiseless input

disp('Press any button to continue')
pause

disp('Checking whether S satisfies Schur independence.')
schur_flag = Check_Schur_indep(S);
if(schur_flag == -1)
    sprintf('S is not Schur independent. Program will terminate now.')
    return;
end

Noise = randn(n); 

disp('Adding Gaussian noise to A.')
bruit = noiselevel * Noise / norm(Noise,'fro') * norm(Ano,'fro');
A = Ano + bruit; 

disp('Projecting A to the elliptope')
cvx_begin sdp quiet
    % Variables 
    variable A_tilde(n,n) 
    % Objectif 
    minimize ( norm(A - A_tilde,'fro') )
    % Contraintes
    subject to
        A_tilde == hermitian_semidefinite( n ); 
        for i = 1 : n
            A_tilde(i,i) == 1;
        end
cvx_end

A = A_tilde;
A_init = A_tilde;

% Default options SSCD, you can play and change them
options = []; 
% options.sscd_algo = 'PG'; % (Default, can be changed to 'IPM')
% options.trace_tol = 3;
% options.epsilon = 5e-3;

disp('Running the SDP-based algorithm for SSCD...'); 
Sign_comps = SSCD(A, r, options);

% Cost is equal to the number of wrong elements retrieved for the 
% binary factor.
[perms,cost,signperms] = mismatchpm1(Sign_comps,S);     

t = pinv(kron(Sign_comps,Sign_comps))*vec(A); % Calculate the tau scalars.

X_approx = zeros(n,n);
for i=1:r
    X_approx = X_approx + (t(i + (i-1)*r).*Sign_comps(:,i)*Sign_comps(:,i).');
end

sym_sign_error = norm(Ano - X_approx,'fro')/norm(Ano,'fro');

fprintf('Relative error of the aproximation: ||A-A''||_F/||A||_F = %2.2f%%\n', 100*sym_sign_error);
fprintf('The number of mismatches between S and S'' is %1.0f\n', cost); 
