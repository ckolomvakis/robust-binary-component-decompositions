% Function that performs all steps of the ABCD algorithm according to [1]. 
% The user specifies how the SDPs will be solved.
%
% *********
%   Input
% *********
% C :   input n-by-m matrix that admits an ABCD. It has been perturbed by noise. 
% Cno:  The noiseless version of C. 
% r:    The rank of the decomposition.
%
% ---Options--- 
% .epsilon      :   the parameter for the trace equality constraint (halfspace 
%                   constraint). Default value is 0.001.
% .trace_tol    :   Tolerance that determines how far from n we can accept 
%                   trace(U'*s_i*s_i'*U) to be. We accept the candidate s_i
%                   if trace(U'*s_i*s_i'*U) >= n - options.trace_tol. Default value 
%                   is set to 3.
% .sscd_algorithm : The algorithm used for the SDP of the SSCD. Default is 'PG'.
% .ascd_algorithm : The algorithm used for the SDP of the ASCD. Default is 'PG'.
%
% Sign_Z: The ground truth binary factor that we will use to compare the 
%         performance of the algorithm.
%
% **********
%   Output
% **********
% ASCD_time : The time needed to compute the Asymmetric Sign Component   
%             Decomposition of 2*C - E, where E is the all ones matrix. 
%             Includes the time for the SSCD algorithm. 
% cost:       The number of wrong binary elements retrieved.  
% rel_error:  The relative error of the reconstructed C with respect to
%             the noiseless input.
% C_approx:   The reconstructed C.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component decomposition 
% part II: the asymmetric case,” Arxiv preprint, 2019
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function [ASCD_time, cost, rel_error, C_approx] = ABCD_algorithm(C, Cno, r, options, Sign_Z)

    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.05;
    end
    if ~isfield(options,'sscd_algo')
        options.sscd_algo = 'PG'; 
    end
    if ~isfield(options,'ascd_algo')
        options.ascd_algo = 'PG'; 
    end

    [n, m] = size(C);

    if(strcmp(options.ascd_algo, 'IPM'))
        tic;
        sprintf('In ASCD IPM')
        [Sign_comps, W_approx] = ASCD_for_ABCD_IPM(C, r, options);
        ASCD_time = toc;
    elseif(strcmp(options.ascd_algo, 'PG'))
        tic;
        sprintf('In ASCD PG')
        [Sign_comps, W_approx] = ASCD_for_ABCD_PG(C, r, options);
        ASCD_time = toc;
    end
    
    % Perform steps 4 - 7 of Algorithm 2 in [1].
    test1 = Sign_comps + 1; % Test if there is a column of all -1.
    idx = find(sum(test1) == 0);
    phi = -1;

    if(isempty(idx))
        test2 = Sign_comps - 1; % Test if there is a column of all +1.
        idx = find(sum(test2) == 0);
        phi = 1;
    end

    if(isempty(idx))
        sprintf('No column of -1 or +1 was found...')
        idx = r;
    end

    W_approx_res = swap_columns(W_approx,idx,r);
    Sign_comps_res = swap_columns(Sign_comps,idx,r);
    e_m = ones(m,1);
    e_n = ones(n,1);
    x = pinv(W_approx_res(:,1:r))*(phi*W_approx_res(:,r+1) + e_m);

    Z_retr = zeros(n,r);
    W_plus = zeros(m,r);
    for i=1:r
        temp_vec = round(1/2*(x(i)*Sign_comps_res(:,i) + e_n));
        max_val = max(temp_vec);
        min_val = min(temp_vec);
        temp_vec( find(temp_vec == max_val) ) = 1;
        temp_vec( find(temp_vec == min_val) ) = 0;
        Z_retr(:,i) = temp_vec;
        W_plus(:,i) = x(i).*W_approx_res(:,i);
    end
    C_approx = Z_retr*W_plus.';
    rel_error = norm(Cno - C_approx, 'fro')/norm(Cno, 'fro');

    Sign_Zretr = 2*Z_retr - 1;
    [~,cost,~] = mismatchpm1(Sign_Zretr,Sign_Z);
    
end

