% Redirect to the proper ASCD function depending on the values of the
% argument 'options': Solve the Asymmetric Sign Component Decomposition
% SDP either through an ipm (using cvx) or through a first order method.
%
% *********
%   Input
% *********
% B : input n-by-m matrix to be factorized. 
% r : factorization rank r 
%
% ---Options--- 
% .epsilon      : the parameter for the trace equality constraint (halfspace 
%                 constraint). Default value is 0.05.
% .trace_tol    : This value is used for the computation of the SSCD of the
%                 retrieved X. Tolerance that determines how far from n 
%                 we can accept trace(U'*s_i*s_i'*U) to be. We accept the 
%                 candidate s_i if trace(U'*s_i*s_i'*U) >= n - trace_tol.
%                 Default value is set to 3.
% .sscd_algo    : = 'PG', Projected Gradient approach from [2]. It is the 
%                    default method.
%                 = 'IPM', Interior point method. We solve using cvx.
% **********
%   Output
% **********
% (Sign_comps, W_approx) : rank-r Asymmetric Sign Component Decomposition 
% of B. Sign_comps is the n x r factor with elements either +1 or -1,
% that was retrieved. W_approx is the real matrix m x r that was 
% calculated.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component 
% decomposition part II: the asymmetric case,” Arxiv
% preprint, 2019.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function [Sign_comps, W_approx] = ASCD(B, r, options)
    
    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.05;
    end
    if ~isfield(options,'sscd_algo')
        options.sscd_algo = 'PG'; 
    end
    if ~isfield(options,'ascd_algo')
        options.ascd_algo = 'PG'; 
    end
    
    if(strcmp(options.ascd_algo,'PG'))
        [Sign_comps, W_approx] = ASCD_PG(B, r, options);
    elseif(strcmp(options.ascd_algo,'IPM'))
        [Sign_comps, W_approx] = ASCD_IPM(B, r, options);
    end
end

