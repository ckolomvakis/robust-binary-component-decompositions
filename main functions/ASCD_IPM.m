% Computes the ASCD decomposition of B where B is an n x m matrix. Here,  
% the SDP for ASCD is solved through cvx, with an IPM.
%
% *********
%   Input
% *********
% B : input n-by-m matrix to be factorized. 
% r : factorization rank r 
%
% ---Options--- 
% .epsilon      : the parameter for the trace equality constraint (halfspace 
%                 constraint). Default value is 0.05.
% .trace_tol    : This value is used for the computation of the SSCD of the
%                 retrieved X. Tolerance that determines how far from n 
%                 we can accept trace(U'*s_i*s_i'*U) to be. We accept the 
%                 candidate s_i if trace(U'*s_i*s_i'*U) >= n - trace_tol.
%                 Default value is set to 3.
% .sscd_algo         : = 'PG', Projected Gradient approach from [2]. It is the 
%                    default method.
%                 = 'IPM', Interior point method. We solve using cvx.
% **********
%   Output
% **********
% (Sign_comps, W_approx) : rank-r Asymmetric Sign Component Decomposition 
% of B. Sign_comps is the n x r factor with elements either +1 or -1,
% that was retrieved. W_approx is the real matrix m x r that was 
% calculated.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component 
% decomposition part II: the asymmetric case,” Arxiv
% preprint, 2019.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function [Sign_comps, W_approx] = ASCD_IPM(B, r, options)
    
    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.05;
    end
    if ~isfield(options,'sscd_algo')
        options.sscd_algo = 'PG'; 
    end

    [n,m] = size(B);
    [Ub,~,~] = svd(B); 
    U = Ub(:,1:r);

    cvx_begin sdp quiet
        variables X(n,n) Y(m,m) 
        minimize ( trace(Y) )
        subject to
            [X B; B.' Y] == hermitian_semidefinite( n + m ); 
            for i = 1 : n
                X(i,i) == 1;
            end
            % We use inequality instead of equality for the constraint
            % below, in order for cvx to solve the problem faster.
            trace( U'*X*U ) >= n*(1 - options.epsilon);   
    cvx_end

    if (strcmp(options.sscd_algo,'IPM'))
        Sign_comps = SSCD_IPM(X, r, options);
    else
        Sign_comps = SSCD_PG(X, r, options);
    end

    W_approx = (pinv(Sign_comps)*B).';
end