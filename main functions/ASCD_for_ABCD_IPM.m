% Computes the ASCD decomposition of 2*C - E, where C is an n x m matrix 
% that admits an ABCD and E is the all ones n x m matrix. Here, the ASCD
% SDP is solved through an IPM (using cvx).
%
% *********
%   Input
% *********
% C : input n-by-m matrix that admits an ABCD. 
% r : factorization rank r 
%
% ---Options--- 
% .epsilon      : the parameter for the trace equality constraint (halfspace 
%                 constraint). Default value is 0.05.
% .trace_tol    : This value is used for the computation of the SSCD of the
%                 retrieved X. Tolerance that determines how far from n 
%                 we can accept trace(U'*s_i*s_i'*U) to be. We accept the 
%                 candidate s_i if trace(U'*s_i*s_i'*U) >= n - trace_tol.
%                 Default value is set to 3.
% .sscd_algo         : = 'PG', Projected Gradient approach from [2]. It is the 
%                    method.
%                 = 'IPM', Interior point method. We solve using cvx.
% **********
%   Output
% **********
% (Sign_comps, W_approx) : rank-r Asymmetric Sign Component Decomposition 
% of 2*C - E, where E is the all ones n x m matrix. Sign_comps is the n x r  
% factor with elements either +1 or -1, that was retrieved. W_approx is the
% real matrix m x r that was calculated.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component 
% decomposition part II: the asymmetric case,” Arxiv
% preprint, 2019.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function [Sign_comps, W_approx] = ASCD_for_ABCD_IPM(C, r, options)
    
    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.05;
    end
    if ~isfield(options,'sscd_algo')
        options.sscd_algo = 'PG'; 
    end
    

    [n,m] = size(C);
    E = ones(n,m);
    B = 2.*C - E;

    [Ub,~,~] = svd(B); 
    U = Ub(:,1:r+1);
    
    % Solving the SDP problem for ASCD (Line 4, of algorithm 1 from [1])
    cvx_begin sdp quiet
        % Variables 
        variables X(n,n) Y(m,m) 
        % Objectif 
        minimize ( trace(Y) )
        % Contraintes
        subject to
            [X B; B.' Y] == hermitian_semidefinite( n + m ); 
            for i = 1 : n
                X(i,i) == 1;
            end
            trace( U'*X*U ) >= n*(1 - options.epsilon);   
    cvx_end

    if (strcmp(options.sscd_algo,'IPM'))
        Sign_comps = SSCD_IPM(X, r+1, options);
    else
        Sign_comps = SSCD_PG(X, r+1, options);
    end
    
    W_approx = (pinv(Sign_comps)*B).';
end