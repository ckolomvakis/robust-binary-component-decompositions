% Computes the ASCD decomposition of 2*C - E, where C is an n x m matrix 
% that admits an ABCD and E is the all ones n x m matrix. Here, the ASCD
% SDP is solved through a first order method.
%
% *********
%   Input
% *********
% C : input n-by-m matrix that admits an ABCD. 
% r : factorization rank r 
%
% ---Options--- 
% .epsilon      : the parameter for the trace equality constraint (halfspace 
%                 constraint). Default value is 0.05.
% .trace_tol    : This value is used for the computation of the SSCD of the
%                 retrieved X. Tolerance that determines how far from n 
%                 we can accept trace(U'*s_i*s_i'*U) to be. We accept the 
%                 candidate s_i if trace(U'*s_i*s_i'*U) >= n - trace_tol.
%                 Default value is set to 3.
% .sscd_algo    : = 'PG', Projected Gradient approach from [2]. It is the 
%                    method.
%                 = 'IPM', Interior point method. We solve using cvx.
% **********
%   Output
% **********
% (Sign_comps, W_approx) : rank-r Asymmetric Sign Component Decomposition 
% of 2*C - E, where E is the all ones n x m matrix. Sign_comps is the n x r  
% factor with elements either +1 or -1, that was retrieved. W_approx is the
% real matrix m x r that was calculated.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component 
% decomposition part II: the asymmetric case,” Arxiv
% preprint, 2019.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function [Sign_comps, W_approx] = ASCD_for_ABCD_PG(C, r, options)
    % Projected gradient for the ASCD SDP. Please read the SCD_PG.m file
    % first, as many variables have been explained there.

    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.05;
    end
    if ~isfield(options,'sscd_algo')
        options.sscd_algo = 'PG'; 
    end
    

    [n,m] = size(C);
    E = ones(n,m);
    B = 2.*C - E;
    
    [Ub,~,~] = svd(B); 
    U = Ub(:,1:r+1);
    
    W = eye(n);       % For the projection onto the set of n x n psd matrices. 
    W_nm = eye(n+m);  % For the projection onto the set of (n+m) x (n+m) psd matrices.
    
    proj_iter = 1;
    Higham_iter = 25;
    X_pg = randn(n,n);
    X_pg = (X_pg + X_pg')/2;

    Y_pg = randn(m,m);
    Y_pg = (Y_pg + Y_pg')/2;

    Y_pg = Y_pg - eye(m); % Gradient step with step size = 1.
    
    % Projection of X_pg to the set of correlation matrices with tr(U^T*U*X_pg) = n*(1 - epsilon).
    while (proj_iter < 25)
        Y_i = X_pg;
        DS = zeros(n,n);
        for iter=1:Higham_iter
            Rk = Y_i - DS;

            temp_vec = W^(-1/2)*Rk*W^(1/2);
            [L,D] = eig(temp_vec, 'vector');
            D = real(D);
            [D, ind] = sort(D, 'descend');
            L = L(:,ind);
            eig_iter = 1;
            psd_proj = zeros(n,n);
            while( D(eig_iter) > 0 && abs( D(eig_iter) ) > 1e-5 && eig_iter < n )
                psd_proj = psd_proj + D(eig_iter)*L(:,eig_iter)*L(:,eig_iter)';
                eig_iter = eig_iter + 1;
            end
            Xk = W^(-1/2)*(psd_proj)*W^(1/2);

            DS = Xk - Rk;

            theta = diag(Xk - eye(n));
            proj_U = Xk - diag(theta);
            Y_i = proj_U;
        end

        u = (trace(U*U'*Y_i) - n*(1 - options.epsilon))/(r+1);
        X_pg = Y_i - u*(U*U');
        proj_iter = proj_iter + 1;
    end

    % Projected the augmented matrix onto the set of (n+m)x(n+m) psd
    % matrices.
    aug_sdp = [X_pg B;B' Y_pg];
    temp_mat = W_nm^(-1/2)*aug_sdp*W_nm^(1/2);
    [L,D] = eig(temp_mat, 'vector');
    [D, ind] = sort(D, 'descend');
    L = L(:,ind);
    eig_iter = 1;
    psd_proj = zeros(n+m,n+m);
    while( D(eig_iter) > 0 )
        psd_proj = psd_proj + D(eig_iter)*L(:,eig_iter)*L(:,eig_iter)';
        eig_iter = eig_iter + 1;
    end
    end_mat = W_nm^(-1/2)*(psd_proj)*W_nm^(1/2);
    
    % Extract the X and Y blocks of the augmented matrix.
    X_test = end_mat(1:n,1:n);
    Y_test = end_mat(n+1:end,n+1:end);
    
    if(strcmp(options.sscd_algo,'IPM'))
        Sign_comps = SSCD_IPM(X_test, r+1, options);
    else
        Sign_comps = SSCD_PG(X_test, r+1, options);
    end
    W_approx = (pinv(Sign_comps)*B).';
end
