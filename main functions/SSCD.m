% Redirect to the proper SSCD function depending on the values of the
% argument 'options': Solve the Symmetric Sign Component Decomposition
% SDP either through an ipm (using cvx) or through a first order method.
%
% *********
%   Input
% *********
% A : input n-by-m matrix to be factorized. 
% r : factorization rank r 
%
% ---Options--- 
% .epsilon      : the parameter for the trace equality constraint (halfspace 
%                 constraint). Default value is 0.05.
% .trace_tol    : This value is used for the computation of the SSCD of the
%                 retrieved X. Tolerance that determines how far from n 
%                 we can accept trace(U'*s_i*s_i'*U) to be. We accept the 
%                 candidate s_i if trace(U'*s_i*s_i'*U) >= n - trace_tol.
%                 Default value is set to 3.
% .sscd_algo    : = 'PG', Projected Gradient approach from [2]. It is the 
%                    default method.
%                 = 'IPM', Interior point method. We solve using cvx.
% **********
%   Output
% **********
% Sign_comps : The Sign components of the rank-r Symmetric Sign Component  
% Decomposition of A. Sign_comps is the n x r factor with elements either  
% +1 or -1, that was retrieved.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component decomposition 
% part I: the positive-semidefinite case,” SIAM Journal on Mathematics of
% Data Science, vol. 3, no. 2, pp. 544–572, 2021.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function Sign_comps = SSCD(A, r, options)

    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.05;
    end
    if ~isfield(options,'sscd_algo')
        options.sscd_algo = 'PG'; 
    end
    
    if(strcmp(options.sscd_algo,'PG'))
        Sign_comps = SSCD_PG(A, r, options);
    elseif(strcmp(options.sscd_algo,'IPM'))
        Sign_comps = SSCD_IPM(A, r, options);
    end
    
end

