% Computes the SSCD decomposition of A, where A is an n x n correlation 
% matrix. Uses an interior point method through cvx.
%
% *********
%   Input
% *********
% S : input n-by-n matrix that admits an SSCD. 
% r : factorization rank r 
%
% ---Options--- 
% .options.epsilon      : the parameter for the trace equality constraint  
%                 (halfspace constraint). Default value is 0.001.
% .options.trace_tol    : Tolerance that determines how far from n we can accept 
%                 trace(U'*s_i*s_i'*U) to be. We accept the candidate s_i
%                 if trace(U'*s_i*s_i'*U) >= n - options.options.trace_tol.
%                 Default value is set to 3.
% **********
%   Output
% **********
% Sign_comps : The Sign components of the rank-r Symmetric Sign Component  
% Decomposition of A. Sign_comps is the n x r factor with elements either  
% +1 or -1, that was retrieved.
%
% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component decomposition 
% part I: the positive-semidefinite case,” SIAM Journal on Mathematics of
% Data Science, vol. 3, no. 2, pp. 544–572, 2021.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function Sign_comps = SSCD_IPM(A, r, options)
    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.001;
    end
    
    n = size(A,2);
    A_init = A;                  % Needed to solve the linear system at the end of the algorithm.
    Sign_comps = zeros(n,r);     % The sign components the algorithm calculates.
    
    retries = zeros(r,1);
    
    max_retries1 = 1;            % For each iteration of the for loop, this is the least
                                 % amount of times of attempts that will be
                                 % made to find a possible vertex.
                             
    max_retries = 30;            % Maximum number of attempts until a suitable vertex is
                                 % retrieved (for each iteration).                       
    
    dists = cell(r,max_retries); % The measure to check if a sign vector retrieved is suitable.
                                 % Here, we use tr(U'*s_i*s_i^t*U) as described in the
                                 % paper.
    
    % Cell variable containing all the sign vectors retrieved for each
    % for iteration, for each attempt.
    Sign_cell = cell(r,max_retries);
    
    % Cell variable containing an index that is 1 if the sign vector
    % retrieved has already been calculated.
    same_vert_cell = cell(r - 1, max_retries);

    ones_flag = 0;               % Flag variable that is 1 whether an all
                                 % ones vector has been found (either +1 or
                                 % -1).

    iden_mat = eye(n);
    [Ua,~,~] = svd(A); 
    
    P = eye(n);                  % Used for the g vector orthogonalization.
    
    U = Ua(:,1:r);               % The U matrix of the cost function.
    for iter=1:r
        
        % Out of the various sign vectors we have computed and stored in
        % Sign_cell, after finishing all the attempts, we have to choose
        % the one that is best suited. max_val indicates the maximum trace value 
        % among the sign vectors we have computed, max_idx indicates at which 
        % position in Sign_cell{iter,:} this sign_vector is located.
        max_idx = -1;
        max_val = -Inf;
        
        while_flag = 1; % Continue or not the attempts to find a suitable 
                        % sign vector.
                        
        distinct_vert_idx = 0;
        
        % The finding of the sign vectors happens in this while.
        while(while_flag == 1)
            % This variable is initially set to zero. If a sufficient
            % vertex has not been found, we set it to one and repeat the
            % while loop. 
            vert_flag = 0;
            
            g = randn(n,1); % The standard normal vector of the cost function...
            g = P*g;        % ... pre-multiplication with P.
            
            cvx_begin sdp quiet
                % Variables 
                variable X(n,n) 
                % Objectif 
                maximize ( g'*X*g )
                % Contraintes
                subject to
                    X == hermitian_semidefinite( n ); 
                    for i = 1 : n
                        X(i,i) == 1;
                    end
            trace( U'*X*U ) == n*(1-options.epsilon);
            cvx_end

            [Ux,~,~] = svd(X);
            
            % Store the sign vector found in Sign_cell.
            Sign_cell{iter,retries(iter) + 1} = sign(Ux(:,1));
            
            % The trace value of the sign vector.
            dists{iter,retries(iter) + 1} = trace(U.'*Sign_cell{iter,retries(iter) + 1}*Sign_cell{iter,retries(iter) + 1}.'*U);

            if(iter == 1)
                retries(iter) = retries(iter) + 1;
                % If the sign vector computed is not satisfactory...
                if( ( n - options.trace_tol > dists{iter,retries(iter)}) && retries(iter) <= max_retries)
                    vert_flag = 1; % ...repeat the while loop.
                end                
                if(dists{iter,retries(iter)} > max_val && vert_flag == 0)
                    max_idx = retries(iter);
                    max_val = dists{iter,retries(iter)};
                end                
            else
                retries(iter) = retries(iter) + 1;
                % Just like in lines 127-129...
                if((n - options.trace_tol > dists{iter,retries(iter)}) && retries(iter) <= max_retries)
                    vert_flag = 1;
                else
                    % Test whether the sign vector has been found again.
                    % Set same_vert_cell{iter - 1, retries(iter)} = 1 if it
                    % is true.
                    for l=iter-1:-1:1
                        if( ( (sum(sign(Ux(:,1)) == Sign_comps(:,l)) == n || sum(sign(Ux(:,1)) == -Sign_comps(:,l)) == n )) && retries(iter) < max_retries)
                            vert_flag = 1;
                            same_vert_cell{iter - 1, retries(iter)} = 1;
                        end
                    end

                    % If I found vertex with a higher trace value than the
                    % max_val already noted, and 1. I have not already found it 
                    % or 2. I have not exhausted the maximum amount of
                    % attempts, then set the max_idx and max_val values
                    % accordingly.
                    if(dists{iter,retries(iter)} > max_val && (retries(iter) > size(same_vert_cell,2) || isempty(same_vert_cell{iter - 1,retries(iter)})))
                        if( ones_flag == 1 && ( sum( Sign_cell{iter,retries(iter)} == ones(n,1) ) == n || sum( Sign_cell{iter,retries(iter)} == -ones(n,1) ) == n ))
                        else
                            max_idx = retries(iter);
                            max_val = dists{iter,retries(iter)};
                        end

%                         if(isempty(same_vert_cell{iter - 1,retries(iter)}) && distinct_vert_idx == 0)
%                             distinct_vert_idx = 1;
%                         end
                    end
                end
            end

            if((vert_flag == 0 || distinct_vert_idx == 1) && retries(iter) >= max_retries1)
                while_flag = 0;
            end
        end

        % If no vertex has been found despite the attempts, then assign
        % one of the previously already found suitable vertices. Do not
        % assign an all ones vector if it has already been found.
        if(max_idx == -1)
            for l=max_retries:-1:1
                    if(ones_flag == 1)
                        if ( ( sum( Sign_cell{iter, l} == ones(n,1) ) == n ) || ( sum( Sign_cell{iter, l} == -ones(n,1) ) == n ) ) 
                        else
                            max_idx = l;
                            max_val = dists{iter, l};
                        end
                    else
                        max_idx = l;
                        max_val = dists{iter, l};
                    end
            end
        end
        
        % Finally assign the vertex found. If it is an all ones vector, set
        % the corresponding flag value to 1.
        Sign_comps(:,iter) = Sign_cell{iter,max_idx};
        if( sum( Sign_comps(:,iter) == ones(n,1) ) == n || sum( Sign_comps(:,iter) == -ones(n,1) ) == n )
            ones_flag = 1;
        end
        
        % Update the P matrix to generate g vectors that are orthogonal to
        % the sign vector assigned.
        P_prev = P*Sign_comps(:,iter); 
        P = (iden_mat - P_prev*P_prev'/norm(P_prev)^2)*P;
    end
end
