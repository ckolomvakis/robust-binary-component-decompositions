% Computes the SSCD decomposition of A, where A is an n x n correlation 
% matrix. Uses the projected gradient approach in [2].
%
% *********
%   Input
% *********
% A : input n-by-n matrix that admits an SSCD. 
% r : factorization rank r 
%
% ---Options--- 
% .epsilon      : the parameter for the trace equality constraint (halfspace 
%                 constraint). Default value is 0.001.
% .trace_tol    : Tolerance that determines how far from n we can accept 
%                 trace(U'*s_i*s_i'*U) to be. We accept the candidate s_i
%                 if trace(U'*s_i*s_i'*U) >= n - options.trace_tol. Default value 
%                 is set to 3.
% **********
%   Output
% **********
% Sign_comps : The Sign components of the rank-r Symmetric Sign Component  
% Decomposition of A. Sign_comps is the n x r factor with elements either  
% +1 or -1, that was retrieved.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component decomposition 
% part I: the positive-semidefinite case,” SIAM Journal on Mathematics of
% Data Science, vol. 3, no. 2, pp. 544–572, 2021.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

function Sign_comps = SSCD_PG(A, r, options)
    % Many of the variables here are already explained in the
    % SCD_noisy_function.m file. The reader is notified for each variable.

    if nargin < 3
        options = [];
    end
    if ~isfield(options,'trace_tol')
        options.trace_tol = 3; 
    end
    if ~isfield(options,'epsilon')
        options.epsilon = 0.001;
    end
    
    n = size(A, 1);
    A_init = A; % Needed to solve the linear system at the end of the algorithm.
    Sign_comps = zeros(n,r); % The sign components the algorithm calculates.
    
    % All of those variables (until line 56) are as defined as in
    % SCD_noisy_function.m
    ones_flag = 0;
    iden_mat = eye(n);
    [U,~,~] = svd(A);
    P = eye(n);
    Ua = U(:,1:r);
    iden_mat = eye(n);
    
    % The variables defined here are for the alternating projections
    % algorithm.
    Y_i = A;                  % As described in [1] (** define it later **)
    Higham_iter = 15;         % Iteration limit for the projection onto the set of 
                              % correlation matrices.
    
    W = eye(n);
    alt_proj_iter_limit = 10; % Alternating projections iteration limit.
    
    grad_limit = 5;           % Gradient algorithm iteration limit.  
    DS = zeros(n);            % As described in [1].
    
    total_while_restarts = 0; % This variable is used to observe the number of 
                              % "re-attempts" to calculate a suitable sign
                              % vector.
                              
    while_limit = 30;         % This is actually the max_retries variable
                              % in SCD_noisy_function.m
    restarts = 0;
    %% Alternating projections
    % Defined as in the SCD_noisy_function.m
    same_vert_flag = 0;
    ones_flag = 0;
    Sign_comps = zeros(n,r);
    for vertex_iter = 1:r
        % Repeat the following while or not
        repeat_flag = 1;  
        while(repeat_flag == 1 && restarts < while_limit)
            % Initialization
            X_proj2 = randn(n,n);
            X_proj2 = (X_proj2 + X_proj2')/2;
            g = randn(n,1);
            g = P*g;
            
            % Step size
            [~,Sg,~] = svd(g*g');
            step = 1/max(Sg(:));
            grad_iter = 1;
            proj_iter = 1;
            while (grad_iter < grad_limit)
                X_proj2 = X_proj2 - step*(-g*g'); % -g*g', because the 
                                                  % original problem is a 
                                                  % maximization problem.
                while (proj_iter < alt_proj_iter_limit)
                    Y_i = X_proj2;
                    for iter=1:Higham_iter
                        Rk = Y_i - DS;

                        temp_vec = W^(1/2)*Rk*W^(1/2);
                        [L,D] = eig(temp_vec, 'vector');
                        D = real(D);
                        [D, ind] = sort(D, 'descend');
                        L = L(:,ind);
                        eig_iter = 1;
                        psd_proj = zeros(n,n);
                        while( D(eig_iter) > 0 && abs( D(eig_iter) ) > 1e-5 && eig_iter < n)
                            psd_proj = psd_proj + D(eig_iter)*L(:,eig_iter)*L(:,eig_iter)';
                            eig_iter = eig_iter + 1;
                        end
                        Xk = W^(-1/2)*(psd_proj)*W^(1/2);

                        DS = Xk - Rk;

                        theta = diag(Xk - eye(n));
                        proj_U = Xk - diag(theta);
                        Y_i = proj_U;
                    end

                    u = (trace(Ua*Ua'*Y_i) - n*(1 - options.epsilon))/r;
                    X_proj2 = Y_i - u*(Ua*Ua');
                    proj_iter = proj_iter + 1;
                end
                grad_iter = grad_iter + 1;
                proj_iter = 1;
            end
            [U_proj,~,~] = svd(X_proj2);
            sign_test = sign(U_proj(:,1));
            trace_test = trace(Ua'*sign_test*sign_test'*Ua);

            if(ones_flag == 1)
                if(sum(sign_test) == n || sum(sign_test) == -n)
                    continue;
                end
            end
                    
            if (vertex_iter > 1)
                for p=vertex_iter-1:-1:1
                    if ( sum(sign_test == Sign_comps(:,p)) == n || sum(sign_test == -Sign_comps(:,p)) == n )
                        same_vert_flag = 1;
                        break;
                    end
                end
            end

            if(trace_test >= n - options.trace_tol && same_vert_flag == 0)
                Sign_comps(:,vertex_iter) = sign_test;
                repeat_flag = 0;
            else
                restarts = restarts + 1;
                total_while_restarts = total_while_restarts + 1;
                same_vert_flag = 0;
                Sign_comps(:,vertex_iter) = sign_test;
            end
        end
        if( sum( Sign_comps(:,vertex_iter) == ones(n,1) ) == n || sum( Sign_comps(:,vertex_iter) == -ones(n,1) ) == n )
            ones_flag = 1;
        end
        P_prev = P*Sign_comps(:,vertex_iter); 

        P = (iden_mat - P_prev*P_prev'/norm(P_prev)^2)*P;
        restarts = 0;
    end
end
