% This code recreates the scalability experiments from the paper 
% "Robust Binary Component Decompositions", by Christos Kolomvakis and 
% Nicolas Gillis. Here, we compare the performance of the interior-point
% method with the performance of our first-order approach. The variables 
% cost_ipm and cost_pg denote the number of false elements retrieved for
% the binary factor. 
%
% The user may specify: the dimensions n and m of B, the rank r, the noise 
% level, the algorithm for SSCD and ASCD, the parameters epsilon and 
% trace_tol.
%
% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component 
% decomposition part II: the asymmetric case,” Arxiv
% preprint, 2019.
% [2]: Christos Kolomvakis and Nicolas Gillis, "Robust Binary Component
% Decompositions", preprint, 2022.

addpath('../utils')
addpath('../main functions')
clear all; 

n = 40; 
m = 40;
r = 3;

Z = round(rand(n,r));
W = randn(m,r);

schur_flag = Check_Schur_indep_binary(Z);
if(schur_flag == -1)
    sprintf('Program will terminate now.')
    return;
end
Sign_Z = 2*Z - 1;
Noise = randn(n,m);
Cno = Z*W.';

noiselevel = 0.05; 
bruit = noiselevel * Noise / norm(Noise,'fro') * norm(Cno,'fro');
C = Cno + bruit; 

options.trace_tol = 3;
options.epsilon = 0.05;
options.sscd_algo = 'IPM';
options.ascd_algo = 'IPM';

[ASCD_time_ipm, cost_ipm, rel_error_ipm, C_approx_ipm] = ABCD_algorithm(C, Cno, r, options, Sign_Z);

options.ascd_algo = 'PG';
options.sscd_algo = 'PG';
[ASCD_time_pg, cost_pg, rel_error_pg, C_approx_pg] = ABCD_algorithm(C, Cno, r, options, Sign_Z);
