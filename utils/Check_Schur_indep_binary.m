% Checks if the binary matrix Z, with elements in {0, 1} and dimensions
% n times r has Schur independent columns.
% *********
%   Input
% *********
% Z : binary input n-by-r matrix.
%
% **********
%   Output
% **********
% retval : Value equal to 1 if Z is Schur independent, or -1 otherwise.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component decomposition 
% part I: the positive-semidefinite case,�? SIAM Journal on Mathematics of
% Data Science, vol. 3, no. 2, pp. 544–572, 2021.
% [2]: Richard Kueng and Joel A. Tropp, “Binary component 
% decomposition part II: the asymmetric case,�? Arxiv
% preprint, 2019.

function retval = Check_Schur_indep_binary(Z)
    [n, r] = size(Z);
    
    Z_check = [ones(n,1) Z];
    for i=1:r
        for j=i+1:r
            Z_check = [Z_check Z(:,i).*Z(:,j)];
        end
    end
    
    if(rank(Z_check) == min(size(Z_check)))
        sprintf('Matrix is Schur independent')
        retval = 1;
    else
        sprintf('Matrix is not Schur independent')
        retval = -1;
    end 
end