% Compute match between two +-1 matrices 
% 
% S1(:,permu) * signperm volumn wise approximates S2 best

function [D,Dsign] = mismatch1col(S,s)

    s(s>=0) = 1; s(s<0) = -1; 
    [m,r] = size(S); 

    D = zeros(r,1); 
    Dsign = ones(r,1); 

    for i = 1 : r
        D(i) = sum( (1 - S(:,i).*s).^2 ) / 4; 
        if D(i) > m-D(i)
            D(i) = m-D(i); 
        	Dsign(i) = -1; 
        end
    end
end