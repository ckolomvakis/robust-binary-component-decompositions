% Compute match between two +-1 matrices 
% 
% S1(:,permu) * signperm volumn wise approximates S2 best

function [permu,cost,signperm] = mismatchpm1(S1,S2)

    [m,r] = size(S1); 

    D = zeros(r,r); 
    Dsign = ones(r,r); 

    for i = 1 : r
        for j = 1 : r
            D(i,j) = sum( (1 - S1(:,i).*S2(:,j)).^2 ) / 4; 
            if D(i,j) > m-D(i,j)
                D(i,j) = m-D(i,j); 
                Dsign(i,j) = -1; 
            end
        end
    end

    [assignment,cost] = munkres(D); 
    [a,permu] = max(assignment); 
    signperm = sum( (assignment.*Dsign)' ); 