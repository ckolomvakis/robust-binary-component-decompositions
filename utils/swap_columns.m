% Simply swap the columns 'idx' and 'r+1' in input matrix A. It is step 4
% of Algorithm 2 (Algorithm for ABCD) in [1].
%
% *********
%   Input
% *********
% A :  input n-by-(r+1) matrix that will have its columns swapped. 
% idx: Column of A to be swapped with column r+1.
% r :  factorization rank r 
% **********
%   Output
% **********
% A_res : Input matrix A, after columns idx and r+1 have been swapped.

% References:
% [1]: Richard Kueng and Joel A. Tropp, “Binary component 
% decomposition part II: the asymmetric case,” Arxiv
% preprint, 2019.

function A_res = swap_columns(A,idx,r)
    v = A(:, idx);
    A(:, idx) = A(:, r+1);
    A(:, r+1) = v;
    A_res = A;
end

